package com.afzaln.gym.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import com.afollestad.materialdialogs.MaterialDialog
import com.afzaln.gym.R
import com.afzaln.gym.adapter.ExercisesAdapter
import com.afzaln.gym.model.TestData
import com.afzaln.kotlin.getToolbar
import com.afzaln.kotlin.inflateMenu
import kotterknife.bindView

class EditWorkoutFragment : Fragment() {
    val TAG = javaClass<EditWorkoutFragment>().getSimpleName()

    val rvExercises: RecyclerView by bindView(R.id.exercise_list)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_edit_workout, container, false)

        return view
    }

    private var adapter: ExercisesAdapter? = null
    var programId: Int = 0
    var workoutId: Int = 0

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)
        getToolbar().setTitle(getResources().getString(R.string.create_program))

        programId = getActivity().getIntent().getIntExtra("programId", 0)
        workoutId = getActivity().getIntent().getIntExtra("workoutId", 0)

        val etWorkoutName: EditText = getToolbar().findViewById(R.id.workout_name) as EditText
        etWorkoutName.setText(TestData.PROGRAMS[programId].workouts[workoutId].name)

        inflateMenu(R.menu.edit_menu)
        getToolbar().setOnMenuItemClickListener(onMenuItemClick)

        val linearLayoutManager = LinearLayoutManager(getActivity())
        rvExercises.setLayoutManager(linearLayoutManager)
        rvExercises.setHasFixedSize(true)

        adapter = ExercisesAdapter(programId, workoutId)
        rvExercises.setAdapter(adapter)
    }

    val onMenuItemClick: (MenuItem) -> Boolean = { item ->
        when (item.getItemId()) {
            R.id.save -> {
                var exercises = TestData.PROGRAMS[programId].workouts[workoutId].exercises

                val checkedExercises = adapter?.checkedExercises

                // crash because if index = 1 gets removed, index = 2 doesn't exist
                for ((key, value) in checkedExercises) {
                    if (value == false) {
                        exercises.remove(key)
                    }
                }

                Log.d(TAG, "saved workout")
                // maybe finish with result to handle in EditProgramActivity/Fragment
                getActivity().finish()
            }
        }

        true
    }
}