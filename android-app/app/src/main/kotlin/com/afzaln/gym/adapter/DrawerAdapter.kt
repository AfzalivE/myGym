package com.afzaln.gym.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import butterknife.ButterKnife
import butterknife.InjectView
import com.afzaln.gym.MainActivity.ItemClickListener
import com.afzaln.gym.R
import kotterknife.bindView

/**
 * Created by afzal on 15-03-29.
 */
public class DrawerAdapter(private val itemClickListener: ItemClickListener) : RecyclerView.Adapter<ViewHolder>() {
    val TAG: String = javaClass<DrawerAdapter>().getSimpleName()

    private val TYPE_HEADER = 0  // Header item type
    private val TYPE_ITEM = 1 // Row item type

    var TITLES = array("Home", "Programs", "Settings")
    var ICONS = intArray(R.drawable.ic_home, R.drawable.ic_programs, R.drawable.ic_settings)

    var NAME = "Oliver Queen"
    var EMAIL = "oqueen@qc.com"
    var AVATAR = R.drawable.avatar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == TYPE_HEADER) {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_header, parent, false)
            val vhItem = HeaderVH(view)
            return vhItem
        } else if (viewType == TYPE_ITEM) {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drawer, parent, false)
            val vhHeader = ItemVH(view, itemClickListener)
            return vhHeader
        }

        throw UnsupportedOperationException("View type: $viewType is not valid")
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewType = getItemViewType(position)

        if (viewType == TYPE_HEADER) {
            val headerVH = holder as HeaderVH
            headerVH.tvName.setText(NAME)
            headerVH.tvEmail.setText(EMAIL)
            headerVH.ivProfile.setImageResource(AVATAR)
        } else if (viewType == TYPE_ITEM) {
            val itemVH = holder as ItemVH
            itemVH.ivRowIcon.setImageResource(ICONS[position - 1])
            itemVH.tvRowText.setText(TITLES[position - 1])
        }
    }

    override fun getItemCount(): Int {
        return TITLES.size() + 1 // plus 1 for header
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return TYPE_HEADER
        }
        return TYPE_ITEM
    }

    public class HeaderVH(itemView: View) : ViewHolder(itemView) {
        // Header views
        val tvName: TextView by bindView(R.id.name)
        val tvEmail: TextView by bindView(R.id.email)
        val ivProfile: ImageView by bindView(R.id.profile_image)
    }

    public class ItemVH(itemView: View, private val itemClickListener: ItemClickListener) : ViewHolder(itemView), View.OnClickListener {

        // Row views
        val ivRowIcon: ImageView by bindView(R.id.row_icon)
        val tvRowText: TextView by bindView(R.id.row_text)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            itemClickListener.onItemClick(view, getAdapterPosition())
        }
    }
}
