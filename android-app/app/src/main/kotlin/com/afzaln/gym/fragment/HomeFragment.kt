package com.afzaln.gym.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afzaln.gym.R
import com.afzaln.gym.activity.CurrentWorkoutActivity
import com.afzaln.gym.adapter.HistoryAdapter
import com.afzaln.kotlin.getToolbar
import com.afzaln.kotlin.startActivityFromFragment
import kotterknife.bindView

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment : Fragment() {
    val TAG = javaClass<HomeFragment>().getSimpleName()

    val rvHistory: RecyclerView by bindView(R.id.history_list)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)

        getToolbar().setTitle(getResources().getString(R.string.app_name))

        val linearLayoutManager = LinearLayoutManager(getActivity())
        rvHistory.setLayoutManager(linearLayoutManager)
        rvHistory.setHasFixedSize(true)

        val adapter = HistoryAdapter(onItemClick)
        rvHistory.setAdapter(adapter)
    }

    val onItemClick: (View, Int, Int) -> Unit = { view, position, type ->
        val extra = Bundle()
        extra.putInt("historyId", position)
        startActivityFromFragment<CurrentWorkoutActivity>(extra)
    }
}