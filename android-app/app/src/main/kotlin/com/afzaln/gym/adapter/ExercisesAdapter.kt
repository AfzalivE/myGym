package com.afzaln.gym.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.afzaln.gym.R
import com.afzaln.gym.model.TestData
import com.afzaln.kotlin.onClick
import kotterknife.bindView
import java.util.HashMap

class ExercisesAdapter(programId: Int, workoutId: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var EXERCISES = TestData.PROGRAMS[programId].workouts[workoutId].exercises
    var checkedExercises = HashMap<Int, Boolean>()

    init {
        for (i in 0..EXERCISES.size() - 1) {
            checkedExercises.put(i, true)
        }
    }

    val onItemClick: (View, Int, Int) -> Unit = { view, position, type ->
        val chkToggle = view.findViewById(R.id.toggle) as CheckBox
        chkToggle.setChecked(!chkToggle.isChecked())
        checkedExercises.put(position, chkToggle.isChecked())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_exercises, parent, false)
        val vhExercise = ExerciseVH(view)

        vhExercise.onClick(onItemClick)
        return vhExercise
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val exerciseVH = holder as ExerciseVH
        exerciseVH.tvExerciseName.setText(EXERCISES[position].name)
        exerciseVH.chkToggle.setOnClickListener { view ->
            checkedExercises.put(position, (view as CheckBox).isChecked())
        }
    }

    override fun getItemCount(): Int {
        return EXERCISES.size() // for Active program view
    }

    public class ExerciseVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvExerciseName: TextView by bindView(R.id.exercise_name)
        val chkToggle: CheckBox by bindView(R.id.toggle)
    }
}