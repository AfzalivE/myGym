package com.afzaln.gym.activity

import android.os.Bundle
import com.afzaln.gym.BaseActivity
import com.afzaln.gym.R
import com.afzaln.gym.fragment.CurrentWorkoutFragment

/**
 * Created by afzal on 15-04-05.
 */
class CurrentWorkoutActivity : BaseActivity(CurrentWorkoutFragment()) {

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha)
        mToolbar.setNavigationOnClickListener({view ->
            onBackPressed()
        })
    }
}