package com.afzaln.gym

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import kotterknife.bindView

open class BaseActivity(val initialFragment : Fragment, val layout: Int = R.layout.activity_base) : AppCompatActivity() {

    val mToolbar: Toolbar by bindView(R.id.toolbar)

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, initialFragment)
                .commit()
    }
}