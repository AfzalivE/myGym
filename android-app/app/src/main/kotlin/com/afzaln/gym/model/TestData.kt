package com.afzaln.gym.model

import com.afzaln.kotlin.from
import java.util.ArrayList
import java.util.Calendar
import java.util.Date

/**
 * Created by afzal on 15-04-04.
 */
class TestData<T>() : ArrayList<T>() {
    companion object {
        fun of<T>(vararg args: T): TestData<T> {
            var results: TestData<T> = TestData()
            args.forEach {
                results.add(it)
            }
            return results
        }

        var PROGRAMS = TestData.of<Program>(
                Program("StrongLifts", TestData.of<Workout>(
                        Workout("Workout A", 0, TestData.of<Exercise>(
                                RepExercise("Squat", 5, 5),
                                RepExercise("Bench Press", 5, 5),
                                RepExercise("Barbell Rows", 5, 5)
                        )),
                        Workout("Workout B", 1, TestData.of<Exercise>(
                                RepExercise("Squat", 5, 5),
                                RepExercise("Overhead Press", 5, 5),
                                RepExercise("Deadlifft", 1, 5)
                        ))
                )),
                Program("Starting Strength", TestData.of<Workout>(
                        Workout("Workout A", 0, TestData.of<Exercise>(
                                RepExercise("Squat", 3, 5),
                                RepExercise("Bench Press", 3, 5),
                                RepExercise("Deadlift", 1, 5)
                        )),
                        Workout("Workout B", 1, TestData.of<Exercise>(
                                RepExercise("Squat", 3, 5),
                                RepExercise("Military Press", 3, 5),
                                RepExercise("Power Clean", 5, 3)
                        ))
                )),
                Program("Ice Cream Fitness", TestData.of<Workout>(
                        Workout("Workout A", 0, TestData.of<Exercise>(
                                RepExercise("Squat", 5, 5),
                                RepExercise("Bench Press", 5, 5),
                                RepExercise("Bent Over Row", 5, 5),
                                RepExercise("Barbell Shrugs", 3, 8),
                                RepExercise("Tricep Extension", 3, 8),
                                RepExercise("Straight Bar Curl", 3, 8),
                                RepExercise("Hyperextensions", 2, 10),
                                RepExercise("Cable Crunches", 3, 10)
                        )),
                        Workout("Workout B", 1, TestData.of<Exercise>(
                                RepExercise("Squat", 5, 5),
                                RepExercise("Deadlift", 1, 5),
                                RepExercise("Military Press", 5, 5),
                                RepExercise("90% Bent Over Rows", 5, 5),
                                RepExercise("Close Grip Bench Press", 3, 8),
                                RepExercise("Straight Bar Curl", 3, 8),
                                RepExercise("Cable Crunches", 3, 10)
                        ))
                ))
        )

        var ACTIVE_PROGRAM = PROGRAMS[0]

        var HISTORY = TestData.of<History>(
                // each history item has a copy of the full workout including reps performed
                History(Date().from(2015, Calendar.APRIL, 2),
                        Workout("Workout A", 0, TestData.of<Exercise>(
                                RepExercise("Squat", 8, 5, 45, hashMapOf(
                                        Pair(1, 5), Pair(2, 5), Pair(3, 5), Pair(4, 5), Pair(5, 5) // reps performed in each set
                                )),
                                RepExercise("Bench Press", 5, 5, 45, hashMapOf(
                                        Pair(1, 5), Pair(2, 5), Pair(3, 5), Pair(4, 5), Pair(5, 5) // reps performed in each set
                                )),
                                RepExercise("Barbell Rows", 5, 5, 45, hashMapOf(
                                        Pair(1, 5), Pair(2, 5), Pair(3, 5), Pair(4, 5), Pair(5, 5) // reps performed in each set
                                ))
                        ))),
                History(Date().from(2015, Calendar.APRIL, 4), // a workout that is not started yet
                        Workout("Workout B", 1, TestData.of<Exercise>(
                                RepExercise("Squat", 5, 5, 45),
                                RepExercise("Overhead Press", 5, 5, 45),
                                RepExercise("Deadlifft", 1, 5, 45)
                        )))
                )
    }


}
