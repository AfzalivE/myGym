package com.afzaln.gym.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afzaln.gym.R
import com.afzaln.gym.adapter.CurrentWorkoutAdapter
import com.afzaln.kotlin.getToolbar
import kotterknife.bindView

/**
 * Created by afzal on 15-04-05.
 */
public class CurrentWorkoutFragment : Fragment() {
    val TAG = javaClass<ProgramsFragment>().getSimpleName()

    val rvPrograms: RecyclerView by bindView(R.id.exercise_list)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_current_workout, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)
        getToolbar().setTitle("Current Workout")

        val historyId = getActivity().getIntent().getIntExtra("historyId", 0)

        val linearLayoutManager = LinearLayoutManager(getActivity())
        rvPrograms.setLayoutManager(linearLayoutManager)
        rvPrograms.setHasFixedSize(true)

        val adapter = CurrentWorkoutAdapter(historyId)
        rvPrograms.setAdapter(adapter)
    }
}