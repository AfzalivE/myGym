package com.afzaln.gym.backup

import android.os.Environment
import android.util.Log
import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.exception.ZipException
import java.io.File

/**
 * Created by afzal on 15-04-24.
 */
public class StrongLiftsImport {
    val TAG = javaClass<StrongLiftsImport>().getSimpleName()

    val sdRoot = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator

    val backupFile = sdRoot + "stronglifts/backup.stronglifts"
    val dest = sdRoot + "stronglifts/tmp"
    val strongliftsDb = "stronglifts.db"

    public fun createTmpDir() {
        val destFolder = File(dest)
        if (!destFolder.exists()) {
            destFolder.mkdir()
        }
    }

    public fun deleteTmpDir() {
        val file = File(dest)
        val myFiles: Array<String>
        if (file.isDirectory()) {
            myFiles = file.list()
            for (i in myFiles.indices) {
                val myFile = File(file, myFiles[i])
                myFile.delete()
            }
        }
        val destFolder = File(dest)
        destFolder.delete()

    }

    public fun extractZip(): Boolean {
        try {
            val filename = backupFile
            val file = ZipFile(filename)
            createTmpDir()
            file.extractAll(dest)
            return true
        } catch (e: ZipException) {
            Log.d(TAG, e.getMessage())
            return false
        }
    }
}