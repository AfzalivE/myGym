package com.afzaln.gym

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import com.afzaln.gym.adapter.DrawerAdapter
import com.afzaln.gym.fragment.HomeFragment
import com.afzaln.gym.fragment.SettingsFragment
import com.afzaln.gym.fragment.ProgramsFragment
import kotterknife.bindView

/**
 * Created by afzal on 15-03-29.
 */

// Call BaseActivity with the default fragment and layout
public class MainActivity : BaseActivity(HomeFragment(), R.layout.activity_main) {
    val TAG = javaClass<MainActivity>().getSimpleName()

    val mDrawerLayout: DrawerLayout by bindView(R.id.drawer_layout)
    val mRecyclerView: RecyclerView by bindView(R.id.options_list)

    inner class ItemClickListener {
        fun onItemClick(view: View, position: Int) {
            when (position) {
                1 -> loadContainerFragment(HomeFragment())
                2 -> loadContainerFragment(ProgramsFragment())
                3 -> loadContainerFragment(SettingsFragment())
            }

            mDrawerLayout.closeDrawer(android.view.Gravity.START)
        }
    }

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val drawerToggle = ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close)

        mDrawerLayout.setDrawerListener(drawerToggle)
        drawerToggle.syncState()

        val linearLayoutManager = LinearLayoutManager(this)
        mRecyclerView.setLayoutManager(linearLayoutManager)
        mRecyclerView.setHasFixedSize(true)

        val adapter = DrawerAdapter(ItemClickListener())
        mRecyclerView.setAdapter(adapter)
    }

    fun loadContainerFragment(fragment: Fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }
}