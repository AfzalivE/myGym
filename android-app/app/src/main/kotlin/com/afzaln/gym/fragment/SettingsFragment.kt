package com.afzaln.gym.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afzaln.gym.R
import com.afzaln.kotlin.getToolbar

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsFragment : Fragment() {
    val TAG = javaClass<SettingsFragment>().getSimpleName()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)

        getToolbar().setTitle(getResources().getString(R.string.settings))
    }
}