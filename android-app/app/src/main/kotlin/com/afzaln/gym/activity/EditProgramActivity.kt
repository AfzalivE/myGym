package com.afzaln.gym.activity

import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import com.afzaln.gym.BaseActivity
import com.afzaln.gym.R
import com.afzaln.gym.fragment.EditProgramFragment
import com.github.clans.fab.FloatingActionButton
import kotterknife.bindView

/**
 * Created by afzal on 15-04-02.
 */
class EditProgramActivity : BaseActivity(EditProgramFragment(), R.layout.activity_edit_program) {
    val TAG = javaClass<EditProgramActivity>().getSimpleName()

    val fabNewWorkout: FloatingActionButton by bindView(R.id.new_workout)

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha)
        mToolbar.setNavigationOnClickListener({view ->
            onBackPressed()
        })

        fabNewWorkout.hide(false)
        val runnable = Runnable {
            fabNewWorkout.show(true)
        }

        Handler().postDelayed(runnable, 200)

        fabNewWorkout.setOnClickListener { view ->
            val newWorkoutDialog = MaterialDialog.Builder(this)
                    .title("Create a workout")
                    .customView(R.layout.dialog_new_workout, false)
                    .positiveText("Save")
                    .callback(object : MaterialDialog.ButtonCallback() {
                        override fun onPositive(dialog: MaterialDialog?) {
                            // TODO add exercise to exercise table
                            Log.d(TAG, "saved workout")
                        }
                    })
                    .show()
        }
    }
}