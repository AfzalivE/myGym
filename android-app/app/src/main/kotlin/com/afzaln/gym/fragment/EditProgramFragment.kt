package com.afzaln.gym.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.afzaln.gym.R
import com.afzaln.gym.activity.EditWorkoutActivity
import com.afzaln.gym.adapter.WorkoutsAdapter
import com.afzaln.gym.model.TestData
import com.afzaln.kotlin.getToolbar
import com.afzaln.kotlin.inflateMenu
import com.afzaln.kotlin.startActivityFromFragmentForResult
import kotterknife.bindView

class EditProgramFragment : Fragment() {
    val TAG = javaClass<EditProgramFragment>().getSimpleName()

    val SELECT_EXERCISES = 0

    var programId = 0

    val rvWorkouts: RecyclerView by bindView(R.id.workout_list)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_edit_program, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)
        getToolbar().setTitle(getResources().getString(R.string.create_program))

        programId = getActivity().getIntent().getIntExtra("programId", 0)
        // handle no programId (meaning it's a new action instead of edit)

        val etProgramName: EditText = getToolbar().findViewById(R.id.program_name) as EditText
        etProgramName.setText(TestData.PROGRAMS[programId].name)

        inflateMenu(R.menu.edit_menu)
        getToolbar().setOnMenuItemClickListener(onMenuItemClick)

        val linearLayoutManager = LinearLayoutManager(getActivity())
        rvWorkouts.setLayoutManager(linearLayoutManager)
        rvWorkouts.setHasFixedSize(true)

        val adapter = WorkoutsAdapter(onItemClick, programId)
        rvWorkouts.setAdapter(adapter)
    }

    val onMenuItemClick: (MenuItem) -> Boolean = { item ->
        when (item.getItemId()) {
            R.id.save -> {
                Log.d(TAG, "saved program")
            }
        }

        true
    }

    val onItemClick: (View, Int, Int) -> Unit = { view, position, type ->
        val extra = Bundle()
        extra.putInt("programId", programId)
        extra.putInt("workoutId", position)
        startActivityFromFragmentForResult<EditWorkoutActivity>(extra, SELECT_EXERCISES)
    }

}