package com.afzaln.gym.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.afzaln.gym.R
import com.afzaln.gym.model.*
import com.afzaln.kotlin.onClick
import kotterknife.bindView

class ProgramsAdapter(val itemClickListener: (view: View, position: Int, type: Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_ACTIVE = 0
    private val TYPE_PROGRAM = 1

    var PROGRAMS = TestData.PROGRAMS
    var ACTIVE_PROGRAM = TestData.ACTIVE_PROGRAM

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ACTIVE) {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.programs_active, parent, false)
            val vhActive = ActiveVH(view)
            vhActive.onClick(itemClickListener)
            return vhActive
        } else if (viewType == TYPE_PROGRAM) {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_programs, parent, false)
            val vhProgram = ProgramVH(view)
            vhProgram.onClick(itemClickListener)
            return vhProgram
        }

        throw UnsupportedOperationException("View type: $viewType is not valid")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = getItemViewType(position)

        if (viewType == TYPE_ACTIVE) {
            val activeVH = holder as ActiveVH
            activeVH.tvProgramName.setText(ACTIVE_PROGRAM.name)
        } else if (viewType == TYPE_PROGRAM) {
            val programVH = holder as ProgramVH
            programVH.tvProgramName.setText(PROGRAMS[position - 1].name)
        }
    }

    override fun getItemCount(): Int {
        return PROGRAMS.size() + 1 // for Active program view
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return TYPE_ACTIVE
        }
        return TYPE_PROGRAM
    }

    public class ProgramVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvProgramName: TextView by bindView(R.id.program_name)
    }

    public class ActiveVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvProgramName: TextView by bindView(R.id.program_name)
    }
}