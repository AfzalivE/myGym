package com.afzaln.gym.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afzaln.gym.R
import com.afzaln.gym.activity.EditProgramActivity
import com.afzaln.gym.adapter.ProgramsAdapter
import com.afzaln.kotlin.getToolbar
import com.afzaln.kotlin.startActivityFromFragment
import com.github.clans.fab.FloatingActionButton
import kotterknife.bindView

/**
 * Created by afzal on 15-03-29.
 */
public class ProgramsFragment : Fragment() {
    val TAG = javaClass<ProgramsFragment>().getSimpleName()

    val rvPrograms: RecyclerView by bindView(R.id.program_list)
    val fabNewProgram: FloatingActionButton by bindView(R.id.new_workout)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_programs, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?): Unit {
        super.onActivityCreated(savedInstanceState)

        getToolbar().setTitle(getResources().getString(R.string.programs))

        val linearLayoutManager = LinearLayoutManager(getActivity())
        rvPrograms.setLayoutManager(linearLayoutManager)
        rvPrograms.setHasFixedSize(true)

        fabNewProgram.hide(false)
        fabNewProgram.show(true)
        fabNewProgram.attachToRecyclerView(rvPrograms)
        fabNewProgram.setOnClickListener { view ->
            startActivityFromFragment<EditProgramActivity>()
        }

        val adapter = ProgramsAdapter(onItemClick)
        rvPrograms.setAdapter(adapter)
    }

    val onItemClick: (View, Int, Int) -> Unit = { view, position, type ->
        val extra = Bundle()
        if (position > 0) {
            extra.putInt("programId", position - 1)
        } else {
            extra.putInt("programId", position)
        }
        startActivityFromFragment<EditProgramActivity>(extra)
    }
}