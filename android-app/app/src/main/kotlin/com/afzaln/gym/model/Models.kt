package com.afzaln.gym.model

import java.util.ArrayList
import java.util.Date

/**
 * Created by afzal on 15-04-04.
 */
data class Program(var name: String, var workouts: ArrayList<Workout>) {

}

data class Workout(var name: String, var order: Int, var exercises: ArrayList<Exercise>) {

}

data class TimedExercise(override var name: String,
                         override var sets: Int, // total sets
                         var time: Int, // time per set
                         override var workoutMap: MutableMap<Int, Int> = hashMapOf(),
                         override var type: Type = Type.TIMED) : Exercise {

}

data class RepExercise(override var name: String,
                       override var sets: Int, // total sets
                       var reps: Int, // reps per set
                       var weight: Int = 0, // stored in lbs
                       override var workoutMap: MutableMap<Int, Int> = hashMapOf(),
                       override var type: Type = Type.REPS) : Exercise {

}

data class History(var date: Date, var workout: Workout) {

}

trait Exercise {
    var name: String
    var sets: Int
    var workoutMap: MutableMap<Int, Int>
    var type: Type
}

enum class Type {
    TIMED
    REPS
}