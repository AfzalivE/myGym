package com.afzaln.gym.adapter

/**
 * Created by afzal on 15-04-05.
 */
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.afzaln.gym.R
import com.afzaln.gym.model.TestData
import com.afzaln.kotlin.onClick
import kotterknife.bindView
import java.text.SimpleDateFormat

class HistoryAdapter(val itemClickListener: (View, Int, Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var HISTORY = TestData.HISTORY

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history, parent, false)
        val vhHistory = HistoryVH(view)
        vhHistory.onClick(itemClickListener)
        return vhHistory
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val workoutVH = holder as HistoryVH
        val dateFormatter = SimpleDateFormat("EEEE, MMMM d")
        val date = dateFormatter.format(HISTORY[position].date)

        workoutVH.tvDate.setText(date)
    }

    override fun getItemCount(): Int {
        return HISTORY.size() // for Active program view
    }

    public class HistoryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvDate: TextView by bindView(R.id.date)
    }
}