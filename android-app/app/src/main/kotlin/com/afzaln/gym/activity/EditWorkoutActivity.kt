package com.afzaln.gym.activity

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import com.afollestad.materialdialogs.MaterialDialog
import com.afzaln.gym.BaseActivity
import com.afzaln.gym.R
import com.afzaln.gym.fragment.EditWorkoutFragment
import com.github.clans.fab.FloatingActionButton
import kotterknife.bindView

/**
 * Created by afzal on 15-04-02.
 */
class EditWorkoutActivity : BaseActivity(EditWorkoutFragment(), R.layout.activity_edit_workout) {
    val TAG = javaClass<EditWorkoutActivity>().getSimpleName()

    val fabNewExercise: FloatingActionButton by bindView(R.id.new_exercise)

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha)
        mToolbar.setNavigationOnClickListener({ view ->
            onBackPressed()
        })

        fabNewExercise.hide(false)
        val runnable = Runnable {
            fabNewExercise.show(true)
        }

        Handler().postDelayed(runnable, 200)

        fabNewExercise.setOnClickListener { view ->
            val newExerciseDialog = MaterialDialog.Builder(this)
                    .title("Create an exercise")
                    .customView(R.layout.dialog_new_exercise, false)
                    .positiveText("Save")
                    .callback(object : MaterialDialog.ButtonCallback() {
                        override fun onPositive(dialog: MaterialDialog?) {
                            // TODO add exercise to exercise table
                            Log.d(TAG, "saved exercise")
                        }
                    })
                    .show()

            val etReps = newExerciseDialog.findViewById(R.id.reps) as EditText
            val etTime = newExerciseDialog.findViewById(R.id.time) as EditText

            val spnType = newExerciseDialog.findViewById(R.id.type) as Spinner

            spnType.setOnItemSelectedListener(object: AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    throw UnsupportedOperationException()
                }

                var firstTime = true

                override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                    if (position == 0) {
                        etReps.setVisibility(View.VISIBLE)
                        etTime.setVisibility(View.GONE)
                        if (!firstTime) {
                            etReps.requestFocus()
                        }
                    } else {
                        etTime.setVisibility(View.VISIBLE)
                        etReps.setVisibility(View.GONE)
                        if (!firstTime) {
                            etTime.requestFocus()
                        }
                    }
                    Log.d(TAG, "Item selected")
                    firstTime = false
                }
            })
        }
    }
}