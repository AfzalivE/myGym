package com.afzaln.gym.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.afzaln.gym.R
import com.afzaln.gym.model.Exercise
import com.afzaln.gym.model.RepExercise
import kotterknife.bindView

/**
 * Created by afzal on 15-04-05.
 */
class SetsAdapter(val exercise: Exercise) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val EXERCISE = exercise

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_current_set, parent, false)
        val vhSet = SetVH(view)
        return vhSet
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val setVH = holder as SetVH
        val setNumber = position + 1 // set starting from 1
        val reps = EXERCISE.workoutMap.get(setNumber) ?: 0
        val totalReps = (EXERCISE as RepExercise).reps

        setVH.btReps.setText("$reps / $totalReps")

        setVH.btReps.setOnClickListener { view ->
            val currentRepCount = EXERCISE.workoutMap.get(position + 1) ?: 0
            if (currentRepCount > 0) {
                EXERCISE.workoutMap.set(position + 1, currentRepCount - 1)
            } else {
                EXERCISE.workoutMap.set(position + 1, (EXERCISE : RepExercise).reps)
            }
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return exercise.sets // number of sets
    }

    public class SetVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val btReps: Button by bindView(R.id.reps)
    }
}