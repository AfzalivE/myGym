package com.afzaln.gym.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.afzaln.gym.R
import com.afzaln.gym.model.TestData
import com.afzaln.kotlin.onClick
import kotterknife.bindView

class WorkoutsAdapter(val itemClickListener: (View, Int, Int) -> Unit, programId: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var WORKOUTS = TestData.PROGRAMS[programId].workouts


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_workouts, parent, false)
        val vhWorkout = WorkoutVH(view)
        vhWorkout.onClick(itemClickListener)
        return vhWorkout
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val workoutVH = holder as WorkoutVH
        workoutVH.tvWorkoutName.setText(WORKOUTS[position].name)
    }

    override fun getItemCount(): Int {
        return WORKOUTS.size() // for Active program view
    }

    public class WorkoutVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvWorkoutName: TextView by bindView(R.id.workout_name)
    }
}