package com.afzaln.gym.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.afzaln.gym.R
import com.afzaln.gym.model.RepExercise
import com.afzaln.gym.model.TestData
import kotterknife.bindView

/**
 * Created by afzal on 15-04-05.
 */
class CurrentWorkoutAdapter(val historyId: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var CURRENT_WORKOUT = TestData.HISTORY[historyId]
    val EXERCISES = CURRENT_WORKOUT.workout.exercises

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_current_workout, parent, false)
        val vhCurrentWorkout = CurrentWorkoutVH(view)
        return vhCurrentWorkout
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val currentWorkoutVH = holder as CurrentWorkoutVH
        currentWorkoutVH.tvExerciseName.setText(EXERCISES[position].name)
        val weight = (EXERCISES[position] as RepExercise).weight
        currentWorkoutVH.tvWeight.setText("$weight lbs")

        val linearLayoutManager = LinearLayoutManager(currentWorkoutVH.rvSets.getContext())
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL)

        val adapter = SetsAdapter(EXERCISES[position])
        currentWorkoutVH.rvSets.setLayoutManager(linearLayoutManager)
        currentWorkoutVH.rvSets.setAdapter(adapter)
    }

    override fun getItemCount(): Int {
        return EXERCISES.size() // for Active program view
    }

    public class CurrentWorkoutVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Row views
        val tvExerciseName: TextView by bindView(R.id.exercise_name)
        val tvWeight : TextView by bindView(R.id.weight)
        val rvSets: RecyclerView by bindView(R.id.set_list)
    }
}