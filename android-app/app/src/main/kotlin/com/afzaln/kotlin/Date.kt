package com.afzaln.kotlin

import java.util.Calendar
import java.util.Date

/**
 * Created by afzal on 15-04-05.
 */
public fun Date.from(year: Int, month: Int, day: Int): Date {
    val calendar : Calendar = Calendar.getInstance()
    calendar.set(year, month, day)
    return calendar.getTime()
}