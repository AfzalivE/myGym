package com.afzaln.kotlin

import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import com.afzaln.gym.BaseActivity

public fun Fragment.getToolbar(): Toolbar {
    return (this.getActivity() as BaseActivity).mToolbar
}

public fun Fragment.inflateMenu(menuRes: Int) {
    getToolbar().getMenu().clear()
    getToolbar().inflateMenu(menuRes)
}
