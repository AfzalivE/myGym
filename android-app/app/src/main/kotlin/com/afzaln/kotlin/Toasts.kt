package com.afzaln.kotlin

import android.app.Fragment
import android.content.Context
import android.support.v4.app
import android.widget.Toast
import android.support.v4.app.Fragment as SupportFragment

/**
 * Created by afzal on 15-03-30.
 */
public fun Context.toast(messageResId: Int) {
    Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show()
}

public fun Context.longToast(messageResId: Int) {
    Toast.makeText(this, messageResId, Toast.LENGTH_LONG).show()
}

public fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

public fun Context.longToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

public fun Fragment.toast(messageResId: Int) {
    Toast.makeText(getActivity(), messageResId, Toast.LENGTH_SHORT).show()
}

public fun Fragment.longToast(messageResId: Int) {
    Toast.makeText(getActivity(), messageResId, Toast.LENGTH_LONG).show()
}

public fun Fragment.toast(message: String) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show()
}

public fun Fragment.longToast(message: String) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show()
}

public fun app.Fragment.toast(messageResId: Int) {
    Toast.makeText(getActivity(), messageResId, Toast.LENGTH_SHORT).show()
}

public fun app.Fragment.longToast(messageResId: Int) {
    Toast.makeText(getActivity(), messageResId, Toast.LENGTH_LONG).show()
}

public fun app.Fragment.toast(message: String) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show()
}

public fun app.Fragment.longToast(message: String) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show()
}
