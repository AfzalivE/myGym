package com.afzaln.gym;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.opencsv.CSVReader;
import junit.framework.TestCase;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by afzal on 15-04-23.
 */
public class ImportStrongliftsTest extends TestCase {

    public void testT() {
        try {
            String file = "app/src/test/resources/spreadsheet-stronglifts.csv";
            CSVReader reader = new CSVReader(new FileReader(file));
            String[] line;
            while ((line = reader.readNext()) != null) {
//                 nextLine[] is an array of values from the line
                String date = line[0];
                String note = line[1];
                String workout = "Workout " + line[2];
                String bodyWeight = line[3];

//                Exercise exercise1 = new RepExercise(line[4], 5, 5, );
//                String
            }
            assertEquals(1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    String backupFile = "app/src/test/resources/backup.stronglifts";
    String dest = "app/src/test/tmp";
    String strongliftsDb = "stronglifts.db";

    @Before
    public void setupDb() {
        try {
//            testFolder.create();
//            File tempFolder = testFolder.newFolder();
            ZipFile file = new ZipFile(backupFile);
            file.extractAll(dest);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    @After
    public void removeExtractedFiles() {
        File file = new File(dest);
        String[] myFiles;
        if (file.isDirectory()) {
            myFiles = file.list();
            for (int i = 0; i < myFiles.length; i++) {
                File myFile = new File(file, myFiles[i]);
                myFile.delete();
            }
        }
    }

    public void testImportFromZip() {
        File file = new File(dest + File.separator + strongliftsDb);

        assertThat(file.exists()).isTrue();
    }

    public void testExtractCustomExercises() {
        JSONArray customExercise = getResults();
        try {
            PrintWriter writer = new PrintWriter(dest + File.separator + "json.txt", "UTF-8");
            writer.print(customExercise.toString());
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private JSONArray getResults() {

        String myPath = dest + File.separator + strongliftsDb;// Set path to your database

        String myTable = "customExercise";//Set name of your table

        SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        String searchQuery = "SELECT  * FROM " + myTable;
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);

        JSONArray resultSet = new JSONArray();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }
}
