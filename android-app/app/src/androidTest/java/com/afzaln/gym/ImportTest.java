package com.afzaln.gym;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.afzaln.gym.backup.StrongLiftsImport;
import junit.framework.TestCase;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by afzal on 15-04-24.
 */
public class ImportTest extends TestCase {

    String sdRoot = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;

    String backupFile = sdRoot + "stronglifts/backup.stronglifts";
    String dest = sdRoot + "stronglifts/tmp";
    String strongliftsDb = "stronglifts.db";

    StrongLiftsImport strongLifts = new StrongLiftsImport();

    @Override
    public void setUp() {}

    @Override
    public void tearDown() {}

    public void testExtractZip() {
        assertThat(strongLifts.extractZip()).isTrue();
    }

    public void testImportedFileExists() {
        File file = new File(dest + File.separator + strongliftsDb);
        assertThat(file.exists()).isTrue();
    }

    public void testExtractCustomExercises() {
        JSONArray customExercise = convertToJson("customExercise");
        try {
            PrintWriter writer = new PrintWriter(dest + File.separator + "json.txt", "UTF-8");
            writer.print(customExercise.toString());
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

//    public void testDeleteTmpDir() {
//        strongLifts.deleteTmpDir();
//        assertThat(new File(dest).exists()).isFalse();
//    }

    private JSONArray convertToJson(String tableName) {
        String myPath = dest + File.separator + strongliftsDb;// Set path to your database
        SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        String searchQuery = "SELECT  * FROM " + tableName;
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);
        JSONArray resultSet = new JSONArray();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }
}
